# Hi #

This are the steps to set up an immediate XRFramework with networking support
to interact with LIDAR content in Unity.


# Steps #


General Gitlab account:

1. Ensure you have a Gitlab account and have generated/added an SSH-key.
    https://docs.gitlab.com/ee/user/ssh.html#generate-an-ssh-key-pair 
    Or lookup some other tutorial to generate an private/public SSH-key pair.

2. If you have 2FA authentication on, you must also create a personal access token and use that as the password instead of the gitlab pw when credentials are 
    being requested. Credentials will be requested when 'Pushing' to a repository. When logged into Gitlab (website), go to:
    https://git.wur.nl/-/profile/personal_access_tokens and genearte an access token, save it somewhere and use it as password.


Starting a new project:


1. Create Unity project from URP template. Unity 2021.3.xx
2. Use packagemanager and add package from url: git@git.wur.nl:wander/wanderlib.git (Check unity console to ensure no access issue have occurred)
3. Add XR Interaction Toolkit from packagemanager: com.unity.xr.interaction.toolkit@2.0.3
4. Go to Assets folder and git bash there (So if project is: ProjectName/Assets, browse to Assets folder, then): git clone git@git.wur.nl:wander/xrframework.git
5. Open the Example or Example2 scene from xrframework. Press play.


Using the pre-assembled project from .zip:

1. Unzip and add the project through UnityHub. Then open it. Everything should work.
2. Open the Example or Example2 scene from xrframework. Press play.