﻿using System;
using UnityEngine;

namespace Wander
{
    /* Static struct for obtaining states in the UI within the XR framework.
     */
    public static class XRUIState
    {
        public static bool RayIsOnUI { get; set; }              /// Set by UIRayEventDispatcher
        public static bool SettingsMenuOn { get; set; }         /// Set by XRSettings
        public static bool CreateOrJoinMenuOn { get; set; }     /// Set by XRCreateOrJoin

        public static bool XRAnyMenuOn
        {
            private set { }
            get
            {
                return SettingsMenuOn || CreateOrJoinMenuOn; /* Todo add other menu's */
            }
        }

        public static event Action<XRSettings> OnSettingsEnabled;
        public static event Action<XRSettings> OnSettingsDisabled;

        public static void CallOnSettingsEnabled(XRSettings settings) { OnSettingsEnabled?.Invoke( settings ); }
        public static void CallOnSettingsDisabled( XRSettings settings ) { OnSettingsDisabled?.Invoke( settings ); }
    }
}
