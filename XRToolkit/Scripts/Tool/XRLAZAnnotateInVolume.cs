//#if WANDER_VR

using LAZNamespace;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using System.Linq;
using TMPro;
using UnityEngine.XR.Interaction.Toolkit;

namespace Wander
{
    [RequireComponent(typeof(LineRenderer))]
    public class XRLAZAnnotateInVolume: MonoBehaviour
    {
        public LAZLoader loader;
        public HelperTexts helperText;
        public InputActionProperty placePointKey;
        public InputActionProperty confirmKey;
        public InputActionProperty nextClassification;
        public GameObject placeVolumePointPrefab;
        public Material [] classifierMats;
        public Material [] volumeMaterials;

        [Header("Point Placing")]
        public float distance = 1.0f;
        public float distChangeLengthSpeed = 0.008f;
        
        private GameObject activePoint;
        private ActionBasedController controller;
        private LineRenderer lineRenderer;
        private List<GameObject> points;
        private GK.ConvexHullCalculator calculator;
        private XRWarningMessage warningMsg;

        // Hull Query
        TraceHullQuery hullQuery;

        private void Awake()
        {
            if (loader == null )
                loader = FindObjectOfType<LAZLoader>();
            warningMsg = FindObjectOfType<XRWarningMessage>( true );
            lineRenderer = GetComponent<LineRenderer>();
            points = new List<GameObject>();
            calculator = new GK.ConvexHullCalculator();
            controller = GetComponent<ActionBasedController>();
        }

        private void OnEnable()
        {
            GetComponent<XRControllerHelperTexts>().SetHelperTexts( helperText );
        }

        private void OnDisable()
        {
            points.ForEach( p => p?.Destroy() );
            points.Clear();
            hullQuery = null;
            activePoint.Destroy();
        }

        void Update()
        {
            if (hullQuery!=null)
            {
                AwaitHullQueryAndProcess();
            }
            else
            {
                PlacePointsAndStartHullQuery();
            }
            UpdateInput();
            DrawBeam();
        }

        public static void AnnotateFromHullQuery(LAZLoader loader, TraceHullQuery _hullQuery, int classification)
        {
            if (_hullQuery == null)
                return;

            if (_hullQuery.done)
            {
                loader.StartGenericTask( () =>
                {
                    HashSet<LAZOctreeCell> uniqueCells = new HashSet<LAZOctreeCell>();

                    // Now we have the indices to the vertices in the cell.
                    for (int i = 0;i < _hullQuery.indices.Count;i++)
                    {
                        var renderer = _hullQuery.indices[i].Item1;
                        var lazCell  = _hullQuery.indices[i].Item2;
                        var vertexId = _hullQuery.indices[i].Item3;

                        uniqueCells.Add( lazCell );

                        // TODO, one issue here: if we apply a change to the cell, it will apply for all renderers pointing to this data.
                        lazCell?.TraverseVertices( ( List<LAZVertex> vertices ) =>
                        {
                            LAZVertex vertex = vertices[vertexId];
                            vertex.classification = (ushort)(classification<<8); // 65535 is scaled to [0-1]. So pre-multiply with 256.
                            vertices[vertexId] = vertex;
                            return true;
                        } );
                    }

                    foreach (var cell in uniqueCells)
                    {
                        if (cell != null && cell.LeafReady)
                        {
                            cell.WriteLeafData();
                            cell.MarkDirty();       // This causes it to reload even though it may not have been unloaded by the loader due to culling.
                        }
                    }
                }, null, true );
            }
        }

        private void AwaitHullQueryAndProcess()
        {
            if (hullQuery.done)
            {
                AnnotateFromHullQuery( loader, hullQuery, XRLAZState.activeClassification );
                hullQuery = null;
            }
        }

        private void PlacePointsAndStartHullQuery()
        {
            // Update active point
            if (activePoint == null )
            {
                activePoint = Instantiate( placeVolumePointPrefab );
                
            }
            activePoint.GetComponent<MeshRenderer>().sharedMaterial = classifierMats[XRLAZState.activeClassification];
            activePoint.transform.position = transform.position + transform.forward * distance;

            // If place point key is pressed, place a point.
            if (placePointKey.action.WasPressedThisFrame())
            {
                var point = Instantiate( placeVolumePointPrefab, activePoint.transform.position, Quaternion.identity );
                point.AddComponent<SphereCollider>();
                point.layer = LayerMask.NameToLayer( "Target" );
                points.Add( point );
            }

            // Always update all materials, possible that one switches to different classification after points have been placed.
            points.ForEach( p =>
            {
                if (p != null)
                {
                    p.GetComponent<MeshRenderer>().sharedMaterial = classifierMats[XRLAZState.activeClassification];
                }
            } );

            // If done with placing points and confirm is pressed.
            if (confirmKey.action.WasPressedThisFrame() )
            {
                if (points.Count( p => p!=null ) < 4 )
                {
                    warningMsg.AddWarning( "A minimum of 4 points is required." );
                }
                else
                {
                    // Build convex hull geometry
                    List<Vector3> verts = new List<Vector3>();
                    List<int> tris = new List<int>();
                    List<Vector3> normals = new List<Vector3>();
                    List<Vector3> hullPoints = points.Where(p=>p!=null).Select( p => p.transform.position ).ToList();
                    calculator.GenerateHull( hullPoints, false, ref verts, ref tris, ref normals );

                    // Create mesh
                    Mesh m = new Mesh();
                    m.SetVertices( verts );
                    m.SetTriangles( tris, 0 );
                    m.SetNormals( normals );

                    // Add geometry to game object
                    GameObject volume = new GameObject("Volume");
                    var meshRenderer = volume.AddComponent<MeshRenderer>();
                    var meshFilter   = volume.AddComponent<MeshFilter>();
                    meshRenderer.sharedMaterial = volumeMaterials[XRLAZState.activeClassification];
                    meshFilter.sharedMesh = m;
                    volume.AddComponent<MeshCollider>();
                    volume.layer = LayerMask.NameToLayer( "Target" );
               //     Destroy( volume, 2 );

                    // Get points inside hull and mark them. Hull query is executed as task in pool.
                    hullQuery = loader.StartHullQuery( verts, tris );

                    // When this object gets destroyed, the annoated points must also reset, for that a special remove routine is required.
                    var removeRoutine = volume.AddComponent<XRLAZRemoveVolume>();
                    removeRoutine.loader = loader;
                    removeRoutine.hullQuery = hullQuery;

                    // Remove points already on main thread.
                    points.ForEach( p => p?.Destroy() );
                    points.Clear();
                }
            }
        }

        private void UpdateInput()
        {
            var joyInputY = controller.translateAnchorAction.action.ReadValue<Vector2>();
            distance += joyInputY.y * distChangeLengthSpeed;
            distance  = Mathf.Clamp( distance, 0.1f, 20f );
            
            if (nextClassification.action.WasPressedThisFrame())
            {
                XRLAZState.activeClassification += 1;
                if (XRLAZState.activeClassification == classifierMats.Length)
                {
                    XRLAZState.activeClassification = 0;
                }
            }
        }

        private void DrawBeam()
        {
            float d = distance;
            lineRenderer.sharedMaterial = classifierMats[XRLAZState.activeClassification];
            lineRenderer.SetPosition( 1, new Vector3( 0, 0, d ) );
            // network
            XRNetworkState.LaserLength[(int)XRHand.Left] = d;
            XRNetworkState.LaserColor[(int)XRHand.Left]  = lineRenderer.sharedMaterial.GetColor( "_Color" );
        }
    }
}

//#endif