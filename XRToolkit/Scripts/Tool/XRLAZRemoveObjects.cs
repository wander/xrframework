//#if WANDER_VR

using LAZNamespace;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Wander
{
    [RequireComponent(typeof(LineRenderer))]
    public class XRLAZRemoveObjects: MonoBehaviour
    {
        public LAZLoader loader;
        public InputActionProperty activateKey;
        public Material beamMat;
        public XRHand handSide = XRHand.Right;
        public HelperTexts helperTexts;

        private LineRenderer lineRenderer;
        private XRControllerHelperTexts controllerHelperTexts;

        private void Awake()
        {
            if (loader == null )
                loader = FindObjectOfType<LAZLoader>();
            lineRenderer = GetComponent<LineRenderer>();
            controllerHelperTexts   = GetComponent<XRControllerHelperTexts>();
        }

        void LateUpdate()
        {
            XRLAZState.TriggeringObject = null;
            var distance = lineRenderer.GetPosition(1).z;
            if (Physics.Raycast( transform.position, transform.forward, out RaycastHit hit, distance, LayerMask.GetMask( "Target" ) ))
            {
                XRLAZState.TriggeringObject = hit.collider.gameObject;
                lineRenderer.sharedMaterial = beamMat;
                XRNetworkState.LaserColor[(int)handSide] = beamMat.GetColor( "_Color" );
                controllerHelperTexts.SetHelperTexts( helperTexts );

                if ( activateKey != null && activateKey.action.WasPressedThisFrame())
                {
                    XRLAZState.TriggeringObject.Destroy();
                }
            }
        }
    }
}

//#endif