//#if WANDER_VR

using LAZNamespace;
using UnityEngine;

namespace Wander
{
    public class XRLAZRemoveVolume: MonoBehaviour
    {
        public LAZLoader loader;
        public TraceHullQuery hullQuery;

        private void OnDestroy()
        {
            if (hullQuery != null && hullQuery.done )
            {
                XRLAZAnnotateInVolume.AnnotateFromHullQuery( loader, hullQuery, 0 ); // Set back to original values.
            }
        }

    }
}

//#endif