using System;
using System.Collections;
using System.IO;
using System.Net;
using Unity.Netcode;
using Unity.Netcode.Transports.UTP;
using UnityEngine;

namespace Wander
{

    /* This script controls the flow of the application. */
    public class XRGameplay : MonoBehaviour
    {
        public bool autoStartInEditor = false;

        NetworkManager netManager;
        UnityTransport transport;
        NetworkDiscovery netDiscovery;

        static bool? _isClone;
        public static bool IsClone
        {
            get
            {
                if (_isClone == null)
                {
                    var path = Application.dataPath + "/../.clone"; // This works with ParrelSync
                    _isClone = File.Exists( path ); // Note this a slow function, cache result.
                }
                return _isClone.Value;
            }
        }

        private void Awake()
        {
            netManager   = GetComponent<NetworkManager>();
            transport    = GetComponent<UnityTransport>();
            netDiscovery = GetComponent<NetworkDiscovery>();
        }

        private void Start()
        {
            if ( Application.isEditor && autoStartInEditor )
            {
                if ( !IsClone )
                {
                    netManager.StartHost();
                }
                else
                {
                    StartCoroutine( ConnectToServerFromFirstOption() );
                }
            }
        }

        private void Update()
        {
        }

        IEnumerator ConnectToServerFromFirstOption()
        {
            while(true)
            {
                yield return new WaitForSeconds( 1 );
                var entries = netDiscovery.GetServerEntries();
                if ( entries.Count != 0 )
                {
                    transport.ConnectionData.Address = entries[0].ip;
                    transport.ConnectionData.Port = entries[0].port;
                    Debug.Log("Tried to connect to: " + transport.ConnectionData.Address + " port: " + entries[0].port );
                    netManager.StartClient();
                    yield break;
                }
                    
            }
        }
    }
}