using Unity.Collections;
using Unity.Netcode;
using UnityEngine;
using TMPro;

namespace Wander
{
    // This state is monitored by XRNetworkPlayer.
    public static class XRNetworkState
    {
        public static Color [] LaserColor  = new Color[2];
        public static float [] LaserLength = new float[2];
    }

    public class XRNetworkPlayer : NetworkBehaviour
    {
        public static XRNetworkPlayer Instance;

        public GameObject body;
        public GameObject head;
        public GameObject leftHand;
        public GameObject rightHand;
        public TextMeshPro playerNameGraphic;

        // Monitor the XR Wander  Object to avoid mingling together network code with the XR.
        GameObject xrHead;
        GameObject xrRightHand;
        GameObject xrLeftHand;

        // XR State
        float lastTimeStateSynced;

        // Shared player states.
        NetworkVariable<FixedString64Bytes> playerName = new NetworkVariable<FixedString64Bytes>( default, NetworkVariableReadPermission.Everyone, NetworkVariableWritePermission.Owner );
        NetworkVariable<Color> playerColor             = new NetworkVariable<Color>( default, NetworkVariableReadPermission.Everyone, NetworkVariableWritePermission.Owner );
        NetworkVariable<float> laserLengthLeft         = new NetworkVariable<float>( default, NetworkVariableReadPermission.Everyone, NetworkVariableWritePermission.Owner );
        NetworkVariable<float> laserLengthRight        = new NetworkVariable<float>( default, NetworkVariableReadPermission.Everyone, NetworkVariableWritePermission.Owner );
        NetworkVariable<Color> laserColorLeft          = new NetworkVariable<Color>( default, NetworkVariableReadPermission.Everyone, NetworkVariableWritePermission.Owner );
        NetworkVariable<Color> laserColorRight         = new NetworkVariable<Color>( default, NetworkVariableReadPermission.Everyone, NetworkVariableWritePermission.Owner );


        private void Awake()
        {
            // Find XR Wander Elements. Monitor these instead of mingling network code in there.
            FindXRGameObject( ref xrHead, "Head" );
            FindXRGameObject( ref xrLeftHand, "LeftHand Controller" );
            FindXRGameObject( ref xrRightHand, "RightHand Controller" );
        }

        public override void OnNetworkSpawn()
        {
            // On spawn, we must copy the initial value. OnValueChange is not called for clients that were already in the game.
            if (!IsOwner)
            {
                UpdatePlayerNameOnGameObject( playerName.Value, playerName.Value );
                UpdatePlayerColorOnGameObject( playerColor.Value, playerColor.Value );
                UpdateLaserColorOnGameObject( laserColorLeft.Value, leftHand );
                UpdateLaserColorOnGameObject( laserColorRight.Value, rightHand );
                leftHand.GetComponent<LineRenderer>().SetPosition( 1, new Vector3( 0, 0, laserLengthLeft.Value ) );
                leftHand.GetComponent<LineRenderer>().SetPosition( 1, new Vector3( 0, 0, laserLengthRight.Value ) );
            }
            else
            {
                var rr = GetComponentsInChildren<Renderer>();
                foreach (var r in rr) r.enabled = false;
            }

            // Hook events for changed values.
            playerName.OnValueChanged += UpdatePlayerNameOnGameObject;
            playerColor.OnValueChanged += UpdatePlayerColorOnGameObject;
            laserLengthLeft.OnValueChanged += ( old, newv )  => leftHand.GetComponent<LineRenderer>().SetPosition( 1, new Vector3( 0, 0, newv ) );
            laserLengthRight.OnValueChanged += ( old, newv ) => rightHand.GetComponent<LineRenderer>().SetPosition( 1, new Vector3( 0, 0, newv ) );
            laserColorLeft.OnValueChanged += ( old, newv )   => UpdateLaserColorOnGameObject( newv, leftHand );
            laserColorRight.OnValueChanged += ( old, newv )  => UpdateLaserColorOnGameObject( newv, rightHand );

            // Set initially player name and color if this player is us.
            ulong id = NetworkObjectId;
            if (IsOwner)
            {
                Instance = this;

                // Player Name
                {
                    var args = NetworkHelper.GetCommandlineArgs();
                    if (args.TryGetValue( "-name", out string playerNameOut ))
                    {
                        playerName.Value = playerNameOut;
                        name = $"{playerName} US (id: {id})";
                    }
                    else
                    {
                        name = $"Player US (id: {id})";
                    }
                }

                // Player Color
                {
                    Color c = MiscUtils.RandomColor(new Color(0.2f,0.2f,0.2f),new Color(1.0f,1.0f,1.0f));
                    playerColor.Value = c;
                }
            }
            else
            {
                name = $"Player Remote (id: {id})";
            }
        }

        public override void OnNetworkDespawn()
        {
            Debug.Log( "Network despawn" );
        }

        public override void OnGainedOwnership()
        {
            Debug.Log( "Got ownership" );
        }

        public override void OnLostOwnership()
        {
            Debug.Log( "Lost ownership" );
        }

        void Update()
        {
            if (IsOwner) // Monitor these and send them trough
            {
                // body
                body.transform.position = xrHead.transform.position;
                body.transform.rotation = xrHead.transform.rotation;
                // left hand
                leftHand.transform.position = xrLeftHand.transform.position;
                leftHand.transform.rotation = xrLeftHand.transform.rotation;
                // right hand
                rightHand.transform.position = xrRightHand.transform.position;
                rightHand.transform.rotation = xrRightHand.transform.rotation;

                if (Time.time - lastTimeStateSynced > 0.3333f)
                {
                    lastTimeStateSynced = Time.time;
                    if (IsOwner)
                    {
                        laserColorLeft.Value   = XRNetworkState.LaserColor[0];
                        laserColorRight.Value  = XRNetworkState.LaserColor[1];
                        laserLengthLeft.Value  = XRNetworkState.LaserLength[0];
                        laserLengthRight.Value = XRNetworkState.LaserLength[1];
                    }
                }
            }
        }

        void UpdatePlayerNameOnGameObject( FixedString64Bytes oldName, FixedString64Bytes newName )
        {
            // if (IsOwner) return;
            playerNameGraphic.text = newName.ToString();
            if (!IsServer)
                Debug.Log( "Received player name client" );
            else
                Debug.Log( "Received player name server" );
        }

        void UpdatePlayerColorOnGameObject( Color oldColor, Color newColor )
        {
            // if (IsOwner) return;
            head.GetComponent<Renderer>().material.color = newColor;
            body.GetComponent<Renderer>().material.color = newColor;
            if (!IsServer)
                Debug.Log( "Received player color client" );
            else
                Debug.Log( "Received player color server" );
        }

        void UpdateLaserColorOnGameObject( Color color, GameObject hand )
        {
            var lr = hand.GetComponent<LineRenderer>();
            lr.material.SetColor( "_Color", color );
            //print( "Updated color on gameobject. Matcount equals: " + Resources.FindObjectsOfTypeAll( typeof( Material ) ).Length );
        }

        void FindXRGameObject( ref GameObject go, string name )
        {
            if (go == null)
            {
                go = GameObject.Find( name );
                if (go == null)
                {
                    Debug.LogError( "Cannot find " + name + "!!" );
                }
            }
        }
    }
}