using UnityEngine;

namespace Wander
{
    /* This scripts helps if there is no XR headset available to test XR. */
    public class XRDebugMove : MonoBehaviour
    {
        public GameObject noXRDebugCam;

        public float panSpeed       = 5000;
        public float rotateSpeed    = 5000;
        public float panSmooth      = 25;
        public float rotateSmooth   = 25;
        public bool lockCursor      = true;

        [Header("Debug")]
        [Tooltip("Enable this to see what would happen in game without editor.")]
        public bool dbgIsGame = false;

        Vector3 panVelocity;
        Vector2 rotateVelocity;

        private void Start()
        {
            if (Application.isEditor && !dbgIsGame)
            {
                // Disable usual camera for XR.
                noXRDebugCam.transform.parent.GetComponent<Camera>().enabled = false;
                noXRDebugCam.SetActive( true );
            }
            else enabled = false;
        }

        private void OnEnable()
        {
            if (Application.isEditor)
            {
                if (lockCursor) Cursor.lockState = CursorLockMode.Locked;
            }
        }

        private void OnDisable()
        {
            if (Application.isEditor && !dbgIsGame)
            {
                if (lockCursor) Cursor.lockState = CursorLockMode.None; 
            }
        }

        void Update()
        {
            
            UpdateRotate();
            UpdatePanning();
        }

        void UpdatePanning()
        {
            float dt = Time.deltaTime;
            float ver = 0;
            float hor = 0;
            float up  = 0;
            ver += (Input.GetKey( KeyCode.W ) || Input.GetKey( KeyCode.UpArrow )) ? 1 : 0;
            ver += (Input.GetKey( KeyCode.S ) || Input.GetKey( KeyCode.DownArrow )) ? -1 : 0;
            hor += (Input.GetKey( KeyCode.A ) || Input.GetKey( KeyCode.LeftArrow )) ? -1 : 0;
            hor += (Input.GetKey( KeyCode.D ) || Input.GetKey( KeyCode.RightArrow )) ? 1 : 0;
            up  += (Input.GetKey( KeyCode.Q )) ? -1 : 0;
            up  += (Input.GetKey( KeyCode.E )) ? 1 : 0;
            Vector3 panAccel = new Vector3( hor, up, ver ) * panSpeed;
            panVelocity += panAccel* dt;
            panVelocity *=  MathUtil.Friction( panSmooth, dt );
            transform.position += transform.TransformDirection( new Vector3(panVelocity.x, 0, panVelocity.z) * dt );
            transform.position += new Vector3( 0, panVelocity.y, 0 ) * dt ; // do up movement in world space
        }

        void UpdateRotate()
        {
            float dt = Time.deltaTime;
            Vector2 rotateAccel = Vector2.zero;
            float x  = Input.GetAxisRaw( "Mouse X" );
            float y  = Input.GetAxisRaw( "Mouse Y" );
            rotateAccel.y = y * rotateSpeed;
            rotateAccel.x = x * rotateSpeed;
            rotateVelocity += rotateAccel * dt;
            rotateVelocity *= MathUtil.Friction( rotateSmooth, dt );
            transform.Rotate( Vector3.up, -rotateVelocity.x * dt, Space.World );
            transform.Rotate( Vector3.right, rotateVelocity.y * dt, Space.Self );
        }
    }
}