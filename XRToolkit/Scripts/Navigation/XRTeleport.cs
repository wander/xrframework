using Unity.XR.CoreUtils;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

namespace Wander
{
    [RequireComponent( typeof( ActionBasedController ) )]
    public class XRTeleport : MonoBehaviour
    {
        public HelperTexts helperTexts;
        public float minBeamLength = 1;
        public float maxBeamLength = 50;
        public float beamIncAdd = 20;
        public float rotateDeg = 45;
        public Material lineMaterial;
        public InputActionProperty backwards;
        public XRHand hand = XRHand.Right;

        private ActionBasedController controller;
        private XROrigin origin;
        private LineRenderer lineRenderer;
        private float beamLength = 20;
        private XRControllerHelperTexts controllerHelperTexts;

        private void Awake()
        {
            origin = FindObjectOfType<XROrigin>();
            controller = GetComponent<ActionBasedController>();
            lineRenderer = GetComponent<LineRenderer>();
            controllerHelperTexts = GetComponent<XRControllerHelperTexts>();
        }

        private void OnEnable()
        {
            controllerHelperTexts.SetHelperTexts( helperTexts );
        }

        void Update()
        {
            // When main menu is on discard movement.
            if ( XRUIState.SettingsMenuOn )
                return;

            var joyInputY = controller.translateAnchorAction.action.ReadValue<Vector2>();
            var joyInputX = controller.rotateAnchorAction.action.ReadValue<Vector2>();

            if (controller.translateAnchorAction.action.WasPressedThisFrame())
            {
                beamLength += Mathf.Sign( joyInputY.y ) * beamIncAdd;
                beamLength  = Mathf.Clamp( beamLength, minBeamLength, maxBeamLength );
                lineRenderer.SetPosition( 1, new Vector3( 0, 0, beamLength ) );
                XRNetworkState.LaserLength[(int)XRHand.Right] = beamLength;
            }

            if (controller.rotateAnchorAction.action.WasPressedThisFrame())
            {
                if (joyInputX.x < -0.5f) origin.transform.Rotate( 0, -45, 0, Space.World );
                if (joyInputX.x > 0.5f) origin.transform.Rotate( 0, 45, 0, Space.World );
            }

            bool forward  = controller.activateActionValue.action.WasPressedThisFrame();
            bool backward = (backwards.action != null && backwards.action.WasPressedThisFrame());
            if ( forward || backward )
            {
                Vector3 from = (transform.position + transform.forward*0.3f);
                float n = backward ? -1 : 1;
                if (from.Raycast( transform.forward * n, beamLength, LayerMask.GetMask( "Default", "Ground" ), out RaycastHit hit ))
                {
                    origin.transform.position = hit.point;
                }
                else
                {
                    origin.transform.position += transform.forward*n*beamLength;
                }
            }

            if (XRLAZState.TriggeringObject == null)
            {
                lineRenderer.sharedMaterial = lineMaterial;
                lineRenderer.SetPosition( 1, new Vector3( 0, 0, beamLength ) );
                XRNetworkState.LaserLength[(int)hand] = beamLength;
                XRNetworkState.LaserColor[(int)hand]  = lineMaterial.GetColor( "_Color" );
                controllerHelperTexts.SetHelperTexts( helperTexts );
            }
        }
    }
}