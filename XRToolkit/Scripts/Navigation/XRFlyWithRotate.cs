using Unity.XR.CoreUtils;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace Wander
{
    // NOTE: This script is outdated!!
    [RequireComponent( typeof( ActionBasedController ) )]
    public class XRFlyWithRotate : MonoBehaviour
    {
        public HelperTexts helperTexts;
        public float moveAccel  = 0.2f;
        public float brakeSpeed = 0.5f;
        public float rotateAccel = 0.2f;
        public float rotateBrake = 0.5f;

        float angleX;
        float angleY;
        Vector3 velocity;
        ActionBasedController controller;
        XROrigin origin;

        private void Awake()
        {
            origin = FindObjectOfType<XROrigin>();
            controller = GetComponent<ActionBasedController>();
        }

        private void OnEnable()
        {
            GetComponent<XRControllerHelperTexts>().SetHelperTexts( helperTexts );
        }

        void Update()
        {
            float accel = controller.activateAction.action.ReadValue<float>();
            var joyInputY = controller.translateAnchorAction.action.ReadValue<Vector2>();
            var joyInputX = controller.rotateAnchorAction.action.ReadValue<Vector2>();

            velocity += origin.transform.forward * accel * moveAccel * Time.deltaTime;
            velocity *= MathUtil.Friction( brakeSpeed, Time.deltaTime );;

            angleX += joyInputY.y * rotateAccel * Time.deltaTime;
            angleY += joyInputX.x * rotateAccel * Time.deltaTime;
            angleX *= MathUtil.Friction( rotateBrake, Time.deltaTime );
            angleY *= MathUtil.Friction( rotateBrake, Time.deltaTime );

            origin.transform.Rotate( angleX, 0, 0, Space.Self );
            origin.transform.Rotate( 0, angleY, 0, Space.World );

            origin.transform.position += velocity;

            //Vector3 accel = new Vector3( joyInputX.x * moveAccel, 0, joyInputY.y * moveAccel );
            //accel = transform.TransformVector( accel );
            //velocity += accel * Time.deltaTime;

            //float brake = MathUtil.Friction( brakeSpeed, Time.deltaTime );
            //velocity *= brake;

            //origin.transform.position += velocity;
        }
    }
}