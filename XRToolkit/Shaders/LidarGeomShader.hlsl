
// Make sure this file is not included twice
#ifndef LIDARGEOM_INCLUDED
#define LIDARGEOM_INCLUDED

// Include helper functions from URP
#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
#include "GeometryHelpers.hlsl"

// This structure is created by the renderer and passed to the Vertex function
// It holds data stored on the model, per vertex
struct Attributes {
    float4 positionOS   : POSITION; // Position in object space
    float2 uv           : TEXCOORD0; // UVs
};
// Other common semantics include NORMAL, TANGENT, COLOR

// This structure is generated by the vertex function and passed to the geometry function
struct VertexOutput {
    float3 positionWS   : TEXCOORD0; // Position in world space
};

// This structure is generated by the geometry function and passed to the fragment function
// Remember the renderer averages these values between the three points on the triangle 
struct GeometryOutput {
    float3 positionWS               : TEXCOORD0; // Position in world space
    float4 positionCS               : SV_POSITION; // Position in clip space
};

// The _MainTex property. The sampler and scale/offset vector is also created
TEXTURE2D( _MainTex ); SAMPLER( sampler_MainTex ); float4 _MainTex_ST;
// The pyramid height property
float _PyramidHeight;

// Vertex functions

VertexOutput Vertex( Attributes input ) {
    // Initialize an output struct
    VertexOutput output = (VertexOutput)0;

    // Use this URP functions to convert position to world space
    // The analogous function for normals is GetVertexNormalInputs
    VertexPositionInputs vertexInput = GetVertexPositionInputs( input.positionOS.xyz );
    output.positionWS = vertexInput.positionWS;

    // TRANSFORM_TEX is a macro which scales and offsets the UVs based on the _MainTex_ST variable
 //   output.uv = TRANSFORM_TEX( input.uv, _MainTex );
    return output;
}

// Geometry functions

GeometryOutput SetupVertex( float3 positionWS ) {
    // Setup an output struct
    GeometryOutput output = (GeometryOutput)0;
    output.positionWS = positionWS;
    // This function calculates clip space position, taking the shadow caster pass into account
    output.positionCS = TransformWorldToHClip( positionWS );
    return output;
}

void SetupAndOutputTriangle( inout TriangleStream<GeometryOutput> outputStream, VertexOutput a ) {
    // Restart the triangle strip, signaling the next appends are disconnected from the last
    outputStream.RestartStrip();
    // Add the output data to the output stream, creating a triangle
    outputStream.Append( SetupVertex( a.positionWS ) );
    outputStream.Append( SetupVertex( a.positionWS + float3(0.01, 0,0) ) );
    outputStream.Append( SetupVertex( a.positionWS + float3(-0.01, 0.01, 0) ) );
}

// We create three triangles from one, so there will be 9 vertices
[maxvertexcount( 3 )]
void Geometry( point VertexOutput input[1], inout TriangleStream<GeometryOutput> outputStream )
{
 //   center.uv = GetTriangleCenter( input.uv, input.uv, input.uv );

    // Create the three triangles.
    // Triangles must wind clockwise or they will not render by default
    SetupAndOutputTriangle( outputStream, input[0] );
  //  SetupAndOutputTriangle( outputStream, inputs[1], inputs[2], center );
  //  SetupAndOutputTriangle( outputStream, inputs[2], inputs[0], center );
}

// Fragment functions

// The SV_Target semantic tells the compiler that this function outputs the pixel color
float4 Fragment( GeometryOutput input ) : SV_Target{

#ifdef SHADOW_CASTER_PASS
    // If in the shadow caster pass, we can just return now
    // It's enough to signal that should will cast a shadow
    return 0;
#else
    // Initialize some information for the lighting function
    InputData lightingInput = (InputData)0;
    lightingInput.positionWS = input.positionWS;
  //  lightingInput.normalWS = input.normalWS; // No need to renormalize, since triangles all share normals
    lightingInput.viewDirectionWS = GetViewDirectionFromPosition( input.positionWS );
    lightingInput.shadowCoord = CalculateShadowCoord( input.positionWS, input.positionCS );

    // Read the main texture
  //  float3 albedo = SAMPLE_TEXTURE2D( _MainTex, sampler_MainTex, input.uv ).rgb;

    // Call URP's simple lighting function
    // The arguments are lightingInput, albedo color, specular color, smoothness, emission color, and alpha

    //    half4 UniversalFragmentBlinnPhong( InputData inputData, half3 diffuse, half4 specularGloss, half smoothness, half3 emission, half alpha, half3 normalTS )
    return UniversalFragmentBlinnPhong( lightingInput, 0.5, (half4)1, 0, 0, 1, (half3)0 );
    // return UniversalFragmentBlinnPhong( lightingInput, albedo, 1, 0, 0, 1 );
#endif
}

#endif